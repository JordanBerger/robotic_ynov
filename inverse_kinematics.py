"""BERGER Jordan
    21/10/2019
    But: programmation inverse
    """

import math
#Déclaration des 3 longueurs composant la patte en mm
L1= 51
L2= 63.7
L3= 93

#Coordonnées du point P3: 
X3=-64.14
Y3=0
Z3=-67.79

Theta2c= -20.69 #Theta2c correspond à la correction à appliquer
Theta3c= 90+Theta2c-5.06 #Theta3c correspond à la correction à appliquer 

d13 = math.sqrt(X3*X3+Y3*Y3)-L1 #projection de L2 et L3 sur la plan xy
theta1= math.acos(X3/(d13+L1))*180/math.pi #Calcul de thetat1, convertit en degres

d=math.sqrt(Z3*Z3+d13*d13) #distance ente P1 et P3

a=math.asin(Z3/d)*180/math.pi #angle entre d et d13
b= (math.acos((L2*L2+d*d-L3*L3)/(2*d*L2)))*180/math.pi #angle entre d et L2

theta4= (math.acos((L2*L2+L3*L3-d*d)/(2*L2*L3)))*180/math.pi #angle entre L2 et L3

theta3=-(180-theta4-Theta3c) #theta3 = 180-theta4 avec la correction appliqué
theta2= -(a+b-Theta2c) #thetat2 = a+b avec la correction appliqué

#Affichage des angles en degres permettant d'obtenir P3
print("Valeur de theta1 : ", theta1)
print("Valeur de theta2 : ", theta2)
print("Valeur de theta3 : ", theta3)